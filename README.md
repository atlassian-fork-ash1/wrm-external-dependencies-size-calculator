# WRM External Dependencies Size Calculator
Calculates size of external dependencies by running the requested external dependencies against a running product instance.

## Usage
You can request the reports as a return value or written to file.

### Use `calculateReport` to get the reports as a data object
The size of the returned data object is:

```
interface IGeneratedReports {
    entryPoints: Array<IEntrypointWebresourceDependenciesDescriptor>;
    sizes: ISizeDictionary,
    externalSizes: Array<IEntryChunkDictionary>,
    dependencies: EntryDependencyDescriptor;
}
```

The first param of the method must be a report generated via (TBD).

### Use `calculateReport` to get the reports as a data object
Writes the reports created to a specified directory. (defaults to `.wrm-analyzer-reports` in the current working directory).

The first param of the method must be a report generated via (TBD).
The second param is optional and describes the directory to which the reports get written.